#include <string.h>
// #include <unistd.h>
#include "driver/gpio.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/event_groups.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event_loop.h"
#include "esp_log.h"
#include "lwip/err.h"
#include "lwip/sys.h"
#include "nvs_flash.h"

// #include "button.c"
#include "hap.h"

#include "lwip/sys.h"
#include "lwip/netdb.h"
#include "lwip/api.h"

#define TAG "SWITCH"

#define ACCESSORY_NAME  "SWITCH"
#define ACCESSORY_NAME2  "SWITCH2"

#define MANUFACTURER_NAME   "YOUNGHYUN"
#define MODEL_NAME  "ESP32_ACC"
#define ARRAY_SIZE(array) (sizeof(array) / sizeof(array[0]))
// #define GPIO_INPUT_IO_0     5
// // #define GPIO_INPUT_IO_1     5
// #define GPIO_INPUT_PIN_SEL  (1ULL<<GPIO_INPUT_IO_0)
// #define ESP_INTR_FLAG_DEFAULT 0

#if 1
#define EXAMPLE_ESP_WIFI_SSID "ARHAM7A"
#define EXAMPLE_ESP_WIFI_PASS "arham@1207"
#endif
// #if 0
// #define EXAMPLE_ESP_WIFI_SSID "NO_RUN"
// #define EXAMPLE_ESP_WIFI_PASS "1qaz2wsx"
// #endif

static gpio_num_t LED_PORT = GPIO_NUM_2;
static gpio_num_t LED_PORT2 = GPIO_NUM_4;
static gpio_num_t LED_PORT3 = GPIO_NUM_15;
static gpio_num_t LED_PORT4 = GPIO_NUM_5;


// static xQueueHandle gpio_evt_queue = NULL;

/* FreeRTOS event group to signal when we are connected & ready to make a request */
static EventGroupHandle_t wifi_event_group;
const int WIFI_CONNECTED_BIT = BIT0;

static void* a;
static void* _ev_handle;
static int led = false;
static bool verify1 = true;
static bool verify2 = true;

void* led_read(void* arg)
{
    printf("[MAIN] LED READ\n");
    return (void*)led;
}

void led_write(void* arg, void* value, int len)
{
    printf("[MAIN] LED WRITE. %d\n", (int)value);

    led = (int)value;
    if (value) {
        led = true;
        gpio_set_level(LED_PORT, 1);
    }
    else {
        led = false;
        gpio_set_level(LED_PORT, 0);
    }

    if (_ev_handle)
        hap_event_response(a, _ev_handle, (void*)led);

    return;
}

void led_notify(void* arg, void* ev_handle, bool enable)
{
    if (enable) {
        _ev_handle = ev_handle;
    }
    else {
        _ev_handle = NULL;
    }
}

// static bool _identifed = false;
void* identify_read(void* arg)
{
    return (void*)true;
}

// static void* a1;
static void* _ev_handle1;
static int led1 = false;
void* led_read2(void* arg)
{
    printf("[MAIN] LED READ\n");
    return (void*)led1;
}

void led_write2(void* arg, void* value, int len)
{
    printf("[MAIN] LED WRITE. %d\n", (int)value);

    led1 = (int)value;
    if (value) {
        led1 = true;
        gpio_set_level(LED_PORT2, 1);
    }
    else {
        led1 = false;
        gpio_set_level(LED_PORT2, 0);
    }

    if (_ev_handle1)
        hap_event_response(a, _ev_handle1, (void*)led1);

    return;
}

void led_notify2(void* arg, void* ev_handle1, bool enable)
{
    if (enable) {
        _ev_handle1 = ev_handle1;
    }
    else {
        _ev_handle1 = NULL;
    }
}

// static bool _identifed2 = false;
void* identify_read2(void* arg)
{
    return (void*)true;
}


void hap_object_init(void* arg)
{
    void* accessory_object = hap_accessory_add(a);
    struct hap_characteristic cs[] = {
        {HAP_CHARACTER_IDENTIFY, (void*)true, NULL, identify_read, NULL, NULL},
        {HAP_CHARACTER_MANUFACTURER, (void*)MANUFACTURER_NAME, NULL, NULL, NULL, NULL},
        {HAP_CHARACTER_MODEL, (void*)MODEL_NAME, NULL, NULL, NULL, NULL},
        {HAP_CHARACTER_NAME, (void*)ACCESSORY_NAME, NULL, NULL, NULL, NULL},
        {HAP_CHARACTER_SERIAL_NUMBER, (void*)"0123456789", NULL, NULL, NULL, NULL},
        {HAP_CHARACTER_FIRMWARE_REVISION, (void*)"1.0", NULL, NULL, NULL, NULL},
    };
    hap_service_and_characteristics_add(a, accessory_object, HAP_SERVICE_ACCESSORY_INFORMATION, cs, ARRAY_SIZE(cs));

    struct hap_characteristic cc[] = {
        {HAP_CHARACTER_ON, (void*)led, NULL, led_read, led_write, led_notify},
    };
    hap_service_and_characteristics_add(a, accessory_object, HAP_SERVICE_SWITCHS, cc, ARRAY_SIZE(cc));
    // ESP_LOGI(TAG, "1");



    void* accessory_object1 = hap_accessory_add(a);
    struct hap_characteristic cs1[] = {
        {HAP_CHARACTER_IDENTIFY, (void*)true, NULL, identify_read2, NULL, NULL},
        {HAP_CHARACTER_MANUFACTURER, (void*)MANUFACTURER_NAME, NULL, NULL, NULL, NULL},
        {HAP_CHARACTER_MODEL, (void*)MODEL_NAME, NULL, NULL, NULL, NULL},
        {HAP_CHARACTER_NAME, (void*)ACCESSORY_NAME2, NULL, NULL, NULL, NULL},
        {HAP_CHARACTER_SERIAL_NUMBER, (void*)"0123456789", NULL, NULL, NULL, NULL},
        {HAP_CHARACTER_FIRMWARE_REVISION, (void*)"1.0", NULL, NULL, NULL, NULL},
    };
    hap_service_and_characteristics_add(a, accessory_object1, HAP_SERVICE_ACCESSORY_INFORMATION, cs1, ARRAY_SIZE(cs1));

    struct hap_characteristic cc1[] = {
        {HAP_CHARACTER_ON, (void*)led1, NULL, led_read2, led_write2, led_notify2},
    };
    hap_service_and_characteristics_add(a, accessory_object1, HAP_SERVICE_SWITCHS, cc1, ARRAY_SIZE(cc1));
    // ESP_LOGI(TAG, "2");

}


static esp_err_t event_handler(void *ctx, system_event_t *event)
{
    switch(event->event_id) {
    case SYSTEM_EVENT_STA_START:
        esp_wifi_connect();
        // ESP_LOGI(TAG, "3");
        break;
    case SYSTEM_EVENT_STA_GOT_IP:
        // ESP_LOGI(TAG, "4");
        ESP_LOGI(TAG, "got ip:%s",
                 ip4addr_ntoa(&event->event_info.got_ip.ip_info.ip));
        xEventGroupSetBits(wifi_event_group, WIFI_CONNECTED_BIT);
        {
            hap_init();

            uint8_t mac[6];
            esp_wifi_get_mac(ESP_IF_WIFI_STA, mac);
            char accessory_id[32] = {0,};
            sprintf(accessory_id, "%02X:%02X:%02X:%02X:%02X:%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
            hap_accessory_callback_t callback;
            callback.hap_object_init = hap_object_init;
            a = hap_accessory_register((char*)ACCESSORY_NAME, accessory_id, (char*)"053-58-197", (char*)MANUFACTURER_NAME, HAP_ACCESSORY_CATEGORY_OTHER, 811, 1, NULL, &callback);
        }
        break;
    case SYSTEM_EVENT_STA_DISCONNECTED:
        esp_wifi_connect();
        xEventGroupClearBits(wifi_event_group, WIFI_CONNECTED_BIT);
        break;
    default:
        break;
    }


    return ESP_OK;
}

// void switch1_fun(){
//     if (led==0)
//     {
//         led=true;
//         gpio_set_level(LED_PORT, 1);
//         printf("ON switch\n");
//     }else{
//         led=false;
//         gpio_set_level(LED_PORT, 0);
//         printf("OFF switch\n");
//     }
//     verify1=false;
//     if (_ev_handle)
//         hap_event_response(a, _ev_handle, (void*)led);
// }

// void switch_fun(){
//     if (led1==0)
//     {
//         led1=true;
//         gpio_set_level(LED_PORT2, 1);
//         printf("ON switch2\n");
//     }else{
//         led1=false;
//         gpio_set_level(LED_PORT2, 0);
//         printf("OFF switch2\n");
//     }
//     verify2=false;
//     if (_ev_handle1)
//         hap_event_response(a, _ev_handle1, (void*)led1);
// }

void wifi_init_sta()
{
    wifi_event_group = xEventGroupCreate();

    tcpip_adapter_init();
    ESP_ERROR_CHECK(esp_event_loop_init(event_handler, NULL) );
    // ESP_ERROR_CHECK(esp_event_loop_init(event_handler2, NULL) );

    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));
    wifi_config_t wifi_config = {
        .sta = {
            .ssid = EXAMPLE_ESP_WIFI_SSID,
            .password = EXAMPLE_ESP_WIFI_PASS
        },
    };

    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA) );
    ESP_ERROR_CHECK(esp_wifi_set_config(ESP_IF_WIFI_STA, &wifi_config) );
    ESP_ERROR_CHECK(esp_wifi_start() );

    ESP_LOGI(TAG, "wifi_init_sta finished.");
    ESP_LOGI(TAG, "connect to ap SSID:%s password:%s",
             EXAMPLE_ESP_WIFI_SSID, EXAMPLE_ESP_WIFI_PASS);
}

#define HDR_200 "\nHTTP/1.1 200 OK\r\nContent-type: text/html\r\n\r\n"

static void http_server_netconn_serve(struct netconn *conn)
{
    struct netbuf *inbuf;
    char *buf;
    u16_t buflen;
    err_t err;
    err = netconn_recv(conn, &inbuf);

    if (err == ERR_OK) {
        netbuf_data(inbuf, (void**)&buf, &buflen);
        int temp1=strncmp(buf, "GET /device1/0",14);
        // printf("device1_0%d", temp1);
        int temp2=strncmp(buf, "GET /device1/1",14);
        // printf("device1_1%d", temp2);
        int temp3=strncmp(buf, "GET /device2/0",14);
        // printf("device2_0%d", temp3);
        int temp4=strncmp(buf, "GET /device2/1",14);
        // printf("device2_1%d", temp4);
        if (temp1 == 0){
            ESP_LOGI(TAG, "device1 OFF\n");
            gpio_set_level(LED_PORT, 0);
            netconn_write(conn, "Device1 OFF", sizeof("Device1 OFF")-1, NETCONN_NOCOPY);
            led=false;
            if (_ev_handle)
                hap_event_response(a, _ev_handle, (void*)led);
        }else if (temp2 ==0)
        {
            // printf("device1_1%d", temp2);
            ESP_LOGI(TAG, "device2 ON\n");
            gpio_set_level(LED_PORT, 1);
            netconn_write(conn, "Device1 ON", sizeof("Device1 ON")-1, NETCONN_NOCOPY);
            led=true;
            if (_ev_handle)
                hap_event_response(a, _ev_handle, (void*)led);

        }
        if (temp3 == 0){
            ESP_LOGI(TAG, "device2 OFF\n");
            gpio_set_level(LED_PORT2, 0);
            netconn_write(conn, "Device2 OFF", sizeof("Device2 OFF")-1, NETCONN_NOCOPY);
            led1=false;
            if (_ev_handle1)
                hap_event_response(a, _ev_handle1, (void*)led1);
        }else if (temp4 ==0)
        {
            // printf("device2_1%d", temp4);
            ESP_LOGI(TAG, "device2 ON\n");
            gpio_set_level(LED_PORT2, 1);
            netconn_write(conn, "Device2 ON", sizeof("Device2 ON")-1, NETCONN_NOCOPY);
            led1=true;
            if (_ev_handle1)
                hap_event_response(a, _ev_handle1, (void*)led1);
        }
        /* send HTTP Ok to client */

        netconn_write(conn, HDR_200, sizeof(HDR_200)-1, NETCONN_NOCOPY);
    }
    /* Close the connection (server closes in HTTP) and clean up after ourself */
    netconn_close(conn);
    netbuf_delete(inbuf);
}

static void http_server(void *pvParameters)
{
    uint32_t port;
    if (pvParameters == NULL){
        port = 80;
    }else{
        port = (uint32_t) pvParameters;
    }
    // printf("\n Creating socket at\n %d \n", (uint32_t) pvParameters);
    struct netconn *conn, *newconn;
    err_t err;
    conn = netconn_new(NETCONN_TCP);
    netconn_bind(conn, NULL,  port);
    netconn_listen(conn);
    do {
        err = netconn_accept(conn, &newconn);
        if (err == ERR_OK) {
            http_server_netconn_serve(newconn);
            netconn_delete(newconn);
        }
    } while(err == ERR_OK);
    netconn_close(conn);
    netconn_delete(conn);
}

void fun2(void *pvParameter){
// void fun2(){
    while(1){
        // sleep(0.5);
        // int t=led_read();
        // printf("%d\n",led);
        // ESP_LOGI(TAG,led);

        vTaskDelay(800 / portTICK_PERIOD_MS);
        int switch1 = gpio_get_level(LED_PORT3);
        printf("GPIO1 %d\n",switch1);
        if (switch1==0 && verify1==true)
        {
            if (led==0)
            {
                led=true;
                gpio_set_level(LED_PORT, 1);
                printf("ON switch\n");
            }else{
                led=false;
                gpio_set_level(LED_PORT, 0);
                printf("OFF switch\n");
            }
            verify1=false;
            if (_ev_handle)
                hap_event_response(a, _ev_handle, (void*)led);

        }else if (switch1==1 && verify1==false){
            // gpio_set_level(LED_PORT, 1);
            // verify1=true;
            // printf("ON switch\n");
            if (led==0)
            {
                led=true;
                gpio_set_level(LED_PORT, 1);
                printf("ON switch\n");
            }else{
                led=false;
                gpio_set_level(LED_PORT, 0);
                printf("OFF switch\n");
            }
            verify1=true;
            if (_ev_handle)
                hap_event_response(a, _ev_handle, (void*)led);
        } 


        int switch2 = gpio_get_level(LED_PORT4);
        printf("GPIO2 %d\n",switch2);
        if (switch2==0 && verify2==true)
        {
            // gpio_set_level(LED_PORT2, 0);
            // verify2=false;
            // printf("OFF switch2\n");
            if (led1==0)
            {
                led1=true;
                gpio_set_level(LED_PORT2, 1);
                printf("ON switch2\n");
            }else{
                led1=false;
                gpio_set_level(LED_PORT2, 0);
                printf("OFF switch2\n");
            }
            verify2=false;
            if (_ev_handle1)
                hap_event_response(a, _ev_handle1, (void*)led1);


        }else if (switch2==1 && verify2==false)
        {
            // gpio_set_level(LED_PORT2, 1);
            // verify2=true;
            // printf("ON switch2\n");
            if (led1==0)
            {
                led1=true;
                gpio_set_level(LED_PORT2, 1);
                printf("ON switch2\n");
            }else{
                led1=false;
                gpio_set_level(LED_PORT2, 0);
                printf("OFF switch2\n");
            }
            verify2=true;
            if (_ev_handle1)
                hap_event_response(a, _ev_handle1, (void*)led1);
        } 
    }
}
void app_main()
{
    ESP_ERROR_CHECK( nvs_flash_init() );

    gpio_pad_select_gpio(LED_PORT);
    gpio_set_direction(LED_PORT, GPIO_MODE_OUTPUT);

    gpio_pad_select_gpio(LED_PORT2);
    gpio_set_direction(LED_PORT2, GPIO_MODE_OUTPUT);

    gpio_pad_select_gpio(LED_PORT3);
    gpio_set_direction(LED_PORT3, GPIO_MODE_INPUT);
    // gpio_get_level(LED_PORT3);
    gpio_pad_select_gpio(LED_PORT4);
    gpio_set_direction(LED_PORT4, GPIO_MODE_INPUT);

    wifi_init_sta();
    xTaskCreate(&http_server, "http_server", 2048, NULL, 5, NULL);
    // fun2();
    xTaskCreate(&fun2, "fun2", 2048, NULL, 5, NULL);

}
